var GoogleMap = (function () {
  this.mapOptions = {
    center: new google.maps.LatLng(52.0066700, 4.3555600),
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  this.dataStruct = {
    user: 9001,
    location: {
      lat: 0,
      lng: 0
    },
    message: null
  };

  this.marker = null;

  this.initialize = function () {
    var that = this;
    this.api = new RequestApi();
    
    this.mapCanvas = document.getElementById('map');

    this.map = new google.maps.Map(this.mapCanvas, this.mapOptions);

    window.gMap = {
      map: this.map,
      mapOptions: this.mapOptions,
      dataStruct: this.dataStruct
    }

    this.map.addListener('click', function (event) {
      var mapCenter = this.getCenter();
      var center = {
          lat: mapCenter.H,
          lng: mapCenter.L
        }

      if (this.marker) {
        this.marker.setMap(null);
        this.marker = null;
      }

      this.marker = new google.maps.Marker({
        position: center,
        zoom: that.mapOptions.zoom
      })
      debugger
      this.marker.setMap(this);
    });
  }

  this.showPosition = function (position) {
    var center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      }

      this.replaceMarker(center);
    }

  this.updateRemoteMap = function (data) {
    var center = data.location;

    this.replaceMarker(center);
  }

  this.replaceMarker = function (center) {
    var map = this.map || window.gMap.map
    var mapOptions = this.mapOptions || window.gMap.mapOptions;

    if (this.marker) {
      this.marker.setMap(null);
      this.marker = null;
    }

    this.marker = new google.maps.Marker({
      position: center,
      zoom: mapOptions.zoom
    })

    this.marker.setMap(map);
    map.setCenter(center);

    var dataStruct = this.dataStruct || window.gMap.dataStruct;
    dataStruct.location = center;

    this.sendLocation(dataStruct);
  };

  this.getLocation = function () {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else {
      x.innerHTML = "Geolocation is not supported by this browser.";
    }
  };

  this.resetLocation = function () {
    var map = new google.maps.Map(mapCanvas, mapOptions);
  };

  this.sendLocation = function (data) {
    this.api.sendLocation(data);
  }

  google.maps.event.addDomListener(window, 'load', this.initialize);

  return {
    showPosition: this.showPosition,
    getLocation: this.getLocation,
    resetLocation: this.resetLocation,
    updateRemoteMap: this.updateRemoteMap,
    replaceMarker: this.replaceMarker
  }

});

window.googleMap = GoogleMap();