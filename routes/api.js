var express = require('express');
var router = express.Router();
var app = express();
var UUID = require('node-uuid');
var Firebase = require('firebase');
var socket = require('../sockets/base');

var APIconnect = (function () {
  this.dbUrl = 'sweltering-inferno-8100.firebaseIO.com/alarm-app/';

  this.initialize = function () {
    this.ref = new Firebase(this.dbUrl);
  }

  this.save = function (packet) {
    var uuid = UUID.v1();
    var packetRef = this.ref.child('packets/'+uuid);

    packetRef.set(packet, this.completionHandler);
    socket.send(packet)
  }

  this.completionHandler = function (error) {
    if (error) {
      console.log(error);
    }
  }

});

var API = new APIconnect();
API.initialize();

// router.get('/client/', function (req, res, next) {
//   console.log(req)
//   db.get({
//       msgId: req.msgId
//   });
//   res.send(200)
// });

router.post('/client/', function (req, res, next) {
  var packet = req.body;

  API.save(packet);

  res.sendStatus(200)
});

// router.get('/portal/', function (req, res, next) {
//   db.get({
//       msgId: req.msgId
//   });
//   res.send(200)
// });

// router.post('/portal/', function (req, res, next) {
//   debugger
//   db.save({
//       userId: req.userId,
//       message: req.message,
//       location: req.location
//   });
// });


module.exports = router;
