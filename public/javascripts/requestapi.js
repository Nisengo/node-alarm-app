var RequestApi = (function () {
  this.url = 'http://' + window.location.host + '/api/client';

  this.initialize = function () {
    //constructor    
    this.client = new XMLHttpRequest();
  }

  this.sendLocation = function (data) {
    if (!data) return;

    this.client.open('POST', this.url, true);

    this.client.setRequestHeader('Content-Type', 'application/json');

    this.client.send(JSON.stringify(data));
    
    if (this.client.status === 200) {
      this.successHandler();
    } else {
      this.failureHandler();
    }
  }

  this.successHandler = function () {
    console.log('Packet sent successfully');
  }

  this.failureHandler = function () {
    console.error('Something went wrong!'); 
  }

  this.initialize();

});

window.RequestApi = RequestApi;