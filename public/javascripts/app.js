angular.module('node-alarm', [])
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
    when('/portal', { 
        templateUrl: 'partials/list.html', 
        controller: MessageListCtl
    }).
    when('/portal/:msgId', {
        templateUrl: 'partials/item.html',
        controller: MsgCtl 
    }).
    when('/new', {
        templateUrl: 'partials/new.html',
        controller: NewMsgCtl
    }).
    otherwise({ redirectTo: '/portal' });
}]);