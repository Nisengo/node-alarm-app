var SocketIO = (function () {

  initialize = function (io) {
    this.io = io;
    this.listen();
  }

  this.listen = function () {
    this.io.on('connection', function (client) {
      client.on('message', function (from, msg) {
        console.log('Message from' + from + ': ' + JSON.stringify(msg));
        io.sockets.emit('broadcast', msg);
      })
    })
  }
  this.send = function (msg) {
    this.io.sockets.emit('addPacket', msg);
  }

  return {
    initialize: initialize,
    listen: listen,
    send: send
  }
});

module.exports = {
  initialize: SocketIO().initialize,
  listen: SocketIO().listen,
  send: SocketIO().send
}
